const express = require('express');
const app = express();
const dotenv= require('dotenv').config();

const port = process.env.PORT;


const router = require('./api/routers/router')

app.use('/index',router);
app.listen(port)