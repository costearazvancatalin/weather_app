const express = require('express');
const router = express.Router();
const controller = require('../controllers/index');


router.get('/',controller.getIndex);
router.get('/about',controller.getAbout);
router.get('/help',controller.getHelp)

module.exports = router; 